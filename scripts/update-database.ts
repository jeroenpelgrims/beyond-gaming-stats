import { parseISO } from "date-fns";
import dotenv from "dotenv";
import { loadZipFile } from "../src/lib/zip";

dotenv.config();
const dbFile = `${process.env.DATA_DIR!}/data.sqlite`;

(async () => {
  const zip = await loadZipFile();
  const entries = Object.entries(zip.files)
    .filter(([path, file]) => file.dir)
    .map((x) => parseISO(x[0]));

  // console.log(
  //   Object.entries(zip.files)
  //     .filter(([path, file]) => file.dir)
  //     .map((x) => parseISO(x[0]))
  // );

  console.log(entries);

  // find data in zip file that's newer than the newest data in the database
  // bulk insert this data

  // parse data
  // store in sqlite database
})();
