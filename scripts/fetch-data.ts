import { formatISO } from "date-fns";
import { loadZipFile, saveZipFile } from "../src/lib/zip";

async function archiveData(visitors: string, forum: string) {
  const zip = await loadZipFile();
  const isoDate = formatISO(new Date());
  const folder = zip.folder(isoDate);
  folder?.file("visitors.html", visitors);
  folder?.file("forum.html", forum);
  await saveZipFile(zip);
}

(async () => {
  const [visitors, forum] = await scrapePages();
  await archiveData(visitors, forum);
})();
