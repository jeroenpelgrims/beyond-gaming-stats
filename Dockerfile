# In the code DATAFILE env variable is used to locate the csv with data
# This also needs to be mounted as a volume.
FROM node:18-alpine
WORKDIR /
COPY . /
RUN npm i --unsafe-perm \
	&& npm run build \
	&& rm -r node_modules

FROM node:18-alpine
WORKDIR /app
COPY --from=0 /build /app
RUN echo "* * * * * node /app/fetch-data.js > /dev/stdout" > /var/spool/cron/crontabs/root
CMD crond -l 8 -L /dev/stdout && node index.js
