import dotenv from "dotenv";
import fs from "fs";
import JSZip from "jszip";

dotenv.config();
const archiveFile = `${process.env.DATA_DIR}/data.zip`;

export async function saveZipFile(zip: JSZip) {
  await new Promise((resolve, reject) => {
    zip
      .generateNodeStream({
        type: "nodebuffer",
        streamFiles: true,
        compression: "DEFLATE",
        compressionOptions: {
          level: 9,
        },
      })
      .pipe(fs.createWriteStream(archiveFile))
      .on("finish", resolve)
      .on("error", reject);
  });
}

export async function loadZipFile() {
  if (!fs.existsSync(archiveFile)) {
    fs.mkdirSync(process.env.DATA_DIR!, { recursive: true });
    await saveZipFile(new JSZip());
  }

  const data = fs.readFileSync(archiveFile);
  return JSZip.loadAsync(data);
}
