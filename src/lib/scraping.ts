async function getPageHtml(path: string) {
  const url = `https://www.beyondgaming.be/${path}/`;
  const response = await fetch(url);
  const html = await response.text();
  return html;
}

async function scrapePages() {
  return await Promise.all([getPageHtml("online"), getPageHtml("forums")]);
}
