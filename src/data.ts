import fs from 'fs';
import path from 'path';
import parse from 'csv-parse/lib/sync';
import { Query, groupData, GroupedData } from './charts';
import moment from 'moment'
import { zip } from "ramda";

let data: DiffDataPoint[] = addDiffs(readData());

setInterval(() => {
  console.log("Reloading data");
  data = addDiffs(readData());
}, 15 * 60 * 1000);

function readData(): DataPoint[] {
  const dataPath = process.env.DATAFILE
    ? path.resolve(process.env.DATAFILE)
    : path.join(__dirname, "../data.csv");
  const csv = fs.readFileSync(dataPath);
  const rawItems = parse(csv, { delimiter: ";", quote: "\"" });
  const data = rawItems.splice(1).map(toDataPoint);
  return data;
}

function addDiffs(items: DataPoint[]): DiffDataPoint[] {
  return zip(items, items.slice(1)).map(([a, b]) => ({
    ...b,
    membersDiff: b.members - a.members,
    messagesDiff: b.messages - a.messages,
    threadsDiff: b.threads - a.threads
  }))
}

export function queryData(query: Query): GroupedData {
  const from = query.from ? moment(query.from) : undefined;
  const to = query.to ? moment(query.to) : undefined;

  const filteredData = data
    .filter(x => from ? x.date.isSameOrAfter(query.from) : true)
    .filter(x => to ? x.date.isSameOrBefore(query.to) : true);

  return groupData(query.groupby, filteredData);
}

export type DataPoint = {
  threads: number;
  messages: number;
  members: number;
  date: moment.Moment;
  onlineTotal: number;
  onlineMembers: number;
  onlineGuests: number;
}

export type DiffDataPoint = {
  threadsDiff: number;
  messagesDiff: number;
  membersDiff: number;
} & DataPoint;

function toDataPoint(line: string[]): DataPoint {
  const [
    threads,
    messages,
    members,
    date,
    onlineTotal,
    onlineMembers,
    onlineGuests
  ] = line;

  return {
    threads: parseInt(threads),
    messages: parseInt(messages),
    members: parseInt(members),
    date: moment(date),
    onlineTotal: parseInt(onlineTotal),
    onlineMembers: parseInt(onlineMembers),
    onlineGuests: parseInt(onlineGuests),
  };
}

// // ramda.zip(data, data.slice(1)).map(([a, b]) => b - a)