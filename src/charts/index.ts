import { ParsedQs } from 'qs';
import { groupBy, sum } from "ramda";
import moment from 'moment'
import { DiffDataPoint } from "../data";

export const colors = {
  red: "rgb(239, 71, 111)",
  yellow: "rgb(255, 209, 102)",
  green: "rgb(6, 214, 160)",
  lightBlue: "rgb(17, 138, 178)",
  darkBlue: "rgb(7, 59, 76)"
};

export type GroupedData = {
  [index: string]: DiffDataPoint[];
};

export function processGroupsBy(
  dataGetter: (p: DiffDataPoint) => number,
  groupedData: GroupedData,
  action: (numbers: number[]) => number
) {
  const keys = Object.keys(groupedData);
  const data = keys
    .map(key => groupedData[key])
    .map(groupItems => action(groupItems.map(dataGetter)));

  return data;
}

export type GroupBy = "hour" | "day" | "week" | "month" | "year";
export type Query = {
  groupby: GroupBy;
  from: moment.Moment | undefined;
  to: moment.Moment | undefined;
}

export function parseQuery(query: ParsedQs): Query {
  const groupby = ["hour", "day", "week", "month", "year"]
    .includes(query.groupby as string)
    ? query.groupby as GroupBy
    : "hour";
  const from = ["", undefined].includes(query.from as string | undefined)
    ? moment().subtract(7, 'days')
    : moment(query.from as string);
  const to = ["", undefined].includes(query.to as string | undefined) ? undefined : moment(query.to as string);

  return { groupby, from, to };
}

export function groupData(groupby: GroupBy, data: DiffDataPoint[]) {
  switch (groupby) {
    case "hour":
      return groupBy(x => x.date.format().slice(0, 16), data);
    case "day":
      return groupBy(x => x.date.format().slice(0, 10), data);
    case "week":
      return groupBy(x => x.date.format("GGGG-[W]WW"), data);
    case "month":
      return groupBy(x => x.date.format("YYYY MMMM"), data);
    case "year":
      return groupBy(x => x.date.format("YYYY"), data);
  }
}