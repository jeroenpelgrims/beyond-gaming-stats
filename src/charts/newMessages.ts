import { colors, GroupedData, processGroupsBy } from ".";
import { ChartConfiguration } from "chart.js";
import { max, sum, min } from "ramda";

export default function newMessagesChartConfig(data: GroupedData): ChartConfiguration {
  const messagesDiffSum = processGroupsBy(x => x.messagesDiff, data, sum);

  return {
    type: 'bar',

    data: {
      labels: Object.keys(data),
      datasets: [
        {
          label: "Aantal nieuwe berichten",
          backgroundColor: messagesDiffSum.map(x => x >= 0 ? colors.green : colors.red),
          data: messagesDiffSum,
          yAxisID: "y-axis-1",
        },
        {
          label: "Totaal aantal berichten",
          borderColor: colors.yellow,
          data: processGroupsBy(x => x.messages, data, numbers => numbers.reduce(max)),
          type: "line",
          yAxisID: "y-axis-2"
        },
      ]
    },

    options: {
      animation: {
        duration: 0
      },
      scales: {
        yAxes: [
          {
            type: "linear",
            display: true,
            position: "left",
            id: "y-axis-1",
            ticks: {
              max: messagesDiffSum.reduce(max) * 3,
              min: messagesDiffSum.reduce(min)
            },
          },
          {
            type: "linear",
            display: true,
            position: "right",
            id: "y-axis-2",
          }
        ]
      }
    }
  };
}