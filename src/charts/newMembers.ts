import { colors, GroupedData, processGroupsBy } from ".";
import { ChartConfiguration } from "chart.js";
import { max, sum } from "ramda";

export default function newMembersChartConfig(data: GroupedData): ChartConfiguration {
  const membersDiffSum = processGroupsBy(x => x.membersDiff, data, sum);
  return {
    type: 'bar',

    data: {
      labels: Object.keys(data),
      datasets: [
        {
          label: "Aantal nieuwe leden",
          backgroundColor: colors.green,
          data: membersDiffSum,
          yAxisID: "y-axis-1",
        },
        {
          label: "Totaal aantal leden",
          borderColor: colors.yellow,
          data: processGroupsBy(x => x.members, data, numbers => numbers.reduce(max)),
          type: "line",
          yAxisID: "y-axis-2"
        },
      ]
    },

    options: {
      animation: {
        duration: 0
      },
      scales: {
        yAxes: [
          {
            type: "linear",
            display: true,
            position: "left",
            id: "y-axis-1",
            ticks: {
              max: membersDiffSum.reduce(max) * 3
            }
          },
          {
            type: "linear",
            display: true,
            position: "right",
            id: "y-axis-2",
          }
        ]
      }
    }
  };
}