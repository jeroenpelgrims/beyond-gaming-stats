import { colors, GroupedData, processGroupsBy } from ".";
import { ChartConfiguration } from "chart.js";
import { max, sum, min } from "ramda";

export default function newMembersChartConfig(data: GroupedData): ChartConfiguration {
  const threadsDiffSum = processGroupsBy(x => x.threadsDiff, data, sum);

  return {
    type: 'bar',

    data: {
      labels: Object.keys(data),
      datasets: [
        {
          label: "Aantal nieuwe threads",
          backgroundColor: threadsDiffSum.map(x => x >= 0 ? colors.green : colors.red),
          data: threadsDiffSum,
          yAxisID: "y-axis-1",
        },
        {
          label: "Totaal aantal threads",
          borderColor: colors.yellow,
          data: processGroupsBy(x => x.threads, data, numbers => numbers.reduce(max)),
          type: "line",
          yAxisID: "y-axis-2"
        },
      ]
    },

    options: {
      animation: {
        duration: 0
      },
      scales: {
        yAxes: [
          {
            type: "linear",
            display: true,
            position: "left",
            id: "y-axis-1",
            ticks: {
              max: threadsDiffSum.reduce(max) * 3,
              min: threadsDiffSum.reduce(min)
            },
          },
          {
            type: "linear",
            display: true,
            position: "right",
            id: "y-axis-2",
          }
        ]
      }
    }
  };
}