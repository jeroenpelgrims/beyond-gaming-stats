import { colors, GroupedData, processGroupsBy } from ".";
import { ChartConfiguration } from "chart.js";
import { sum } from "ramda";

export default function onlineMembersChartConfig(data: GroupedData): ChartConfiguration {
  return {
    type: 'line',

    data: {
      labels: Object.keys(data),
      datasets: [
        {
          label: "Online leden",
          borderColor: colors.darkBlue,
          data: processGroupsBy(x => x.onlineMembers, data, sum)
        },
        {
          label: "Online totaal",
          borderColor: colors.lightBlue,
          data: processGroupsBy(x => x.onlineTotal, data, sum)
        }
      ]
    },

    options: {
      animation: {
        duration: 0
      }
    }
  };
}