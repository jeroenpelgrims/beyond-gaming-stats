import path from 'path';
import express from 'express';
import expressLayouts from 'express-ejs-layouts';
import ejs from "ejs";
import { parseQuery } from './charts';
import onlineMembersChartConfig from "./charts/onlineMembers";
import newMembersChartConfig from "./charts/newMembers";
import newThreadsChartConfig from "./charts/newThreads";
import newMessagesChartConfig from "./charts/newMessages";
import { queryData } from './data';

const app = express();

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.engine('ejs', (ejs as any).__express); // Otherwise Wbpack is difficult. (Dynamic import)
app.use(expressLayouts);

app.get("/", (req, res) => {
  const query = parseQuery(req.query);
  const data = queryData(query);

  res.render("index", {
    from: query.from?.format().slice(0, 10),
    to: query.to?.format().slice(0, 10),
    groupby: query.groupby,
    onlineMembersChartConfig: onlineMembersChartConfig(data),
    newMembersChartConfig: newMembersChartConfig(data),
    newThreadsChartConfig: newThreadsChartConfig(data),
    newMessagesChartConfig: newMessagesChartConfig(data)
  });
});

const port = process.env.PORT || 3001;
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});